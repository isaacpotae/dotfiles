#!/bin/sh
# a basic script for easy arch installation.
echo "Prepare the storage. UEFI is assumed. A internet connection is assumed"

echo "enter a hostname:"
read hostname -r
echo "which microcode do you want? [i, I, intel, INTEL] [a, A, amd, AMD] [default, anything else, nothing]"
read microcode -r

pacman -Syy archlinux-keyring

timedatectl set-ntp true

pacstrap /mnt base base-devel linux linux-firmware nano man-db man-pages iwd

genfstab -U /mnt >> /etc/fstab

arch-chroot /mnt /bin/sh -x << EOF 
ln -sf /usr/share/zoneinfo/Pacific/Auckland /etc/localtime
hwclock --systohc
echo " 
en_US.UTF-8 UTF-8" >> /etc/locale.gen 
locale-gen 
touch /etc/locale.conf
echo " 
LANG=en_US.UTF-8" >> /etc/locale.conf
touch /etc/hostname
echo "$hostname" >> /etc/hostname
touch /etc/hosts
echo "127.0.0.1	localhost
::1	localhost
127.0.1.1	$hostname" >> /etc/hosts
mkdir /etc/iwd
touch /etc/iwd/main.conf
echo "[General]
EnableNetworkConfiguration=true

[Network]
RoutePriorityOffset=300
NameResolvingService=systemd" >> /etc/iwd/main.conf
systemctl enable iwd
systemctl enable systemd-resolved
echo "setup a root password:"
EOF
arch-chroot /mnt passwd
case $microcode in
	"i"|"I"|"intel"|"INTEL")
		arch-chroot /mnt pacman -S intel-ucode;;
	"a"|"A"|"amd"|"AMD")
		arch-chroot /mnt pacman -S amd-ucode;;
	*)
		;;
esac
arch-chroot /mnt pacman -S grub efibootmgr
arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

echo "installation complete! reboot when ready"
