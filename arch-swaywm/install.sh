#!/bin/sh
# this script assumes a basic arch install has been completed
 
sudo pacman -S git

git clone https://aur.archlinux.org/yay.git
cd yay || exit
makepkg -si

yay -S sway ttf-font-awesome waybar neovim grim slurp pulseaudio pamixer brightnessctl kitty wofi auto-cpufreq xorg-xwayland librewolf

pulseaudio --daemonize=TRUE
systemctl enable --now auto-cpufreq

cd .. || exit

git clone https://isaacpotae@bitbucket.org/isaacpotae/dotfiles.git

cd dotfiles/arch-swaywm || exit
mv sway/ ~/.config/
mv waybar/ ~/.config/

cat ./bashrc >> ~/.bashrc
cat ./inputrc >> ~/.inputrc
