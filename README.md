# Isaac Potae's Dot Files #

Feel free to use these dot files as you wish. I don't own the rights to any of the wallpapers and don't recall where I found them, so use then at your own discretion. Files have their own place to be set, so be sure to use the rest of this read me to find where they should go if you are unfamiliar with their respective program.

### Purpose ###

* Quick access to dot files for easy config setup
* Make it easier for me when reinstalling/ setting up new machines

### How to Use ###

##### Arch Swaywm Chess #####
* the following directories go into ~/.config:
	* sway
	* waybar

### Housekeeping ###

* This repo is a sole project of Isaac Potae and does not accept contributions of any kind.
* You can get in touch with me via email at isaacpotae@protonmail.com
* You can find out more about me at isaacpotae.co.nz
